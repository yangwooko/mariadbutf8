Files to generate **Docker** image of MariaDB with UTF8 as default collating character set.

You can get generated docker image at [**yangwooko/mariadbutf8 @ dockerhub**](https://hub.docker.com/repository/docker/yangwooko/mariadbutf8).

For detail information, you'd better refer to base docker documentation at [**mariadb @ Docker Official Images**](https://hub.docker.com/_/mariadb).